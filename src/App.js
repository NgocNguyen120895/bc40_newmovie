import {BrowserRouter, Route, Routes} from 'react-router-dom';
import Layout from './Pages/Components/Layout/Layout';
import HomePage from './Pages/HomePage/HomePage';
import NotFoundPage from './Pages/NotFoundPage/NotFoundPage';
import LoginPage from './Pages/LoginPage/LoginPage';
import DetailPage from './Pages/DetailPage/DetailPage';
import RegisterPage from './Pages/RegisterPage/RegisterPage';
import ContactPage from './Pages/ContactPage/ContactPage';
import News from './Pages/NewsPage/News';



export default function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path='*' element={<Layout Component={<NotFoundPage />} />} />
          <Route path='/' element={<Layout Component={<HomePage />} />} />
          <Route path='/login' element={<Layout Component={<LoginPage />} />} />
          <Route
            path='/register'
            element={<Layout Component={<RegisterPage />} />}
          />
          <Route
            path='/detail/:id'
            element={<Layout Component={<DetailPage />} />}
          />
          <Route
            path='/contact'
            element={<Layout Component={<ContactPage />} />}
          />
          <Route path='/news' element={<Layout Component={<News />} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
