import React from 'react';
import BannerMovies from '../Components/CarouselBanner/BannerMovies';
import TabMovie from './TabMovie/TabMovie';
import MoviesCarousel from './MoviesCarousel/MoviesCarousel';

export default function HomePage() {
  return (
    <>
      <BannerMovies />
      <MoviesCarousel />
      <TabMovie />
    </>
  );
}
