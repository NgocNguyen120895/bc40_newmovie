import React from 'react';
import {Card, Image} from 'antd';

export default function CardMovie(props) {
  const {movie} = props;

  return (
    <>
      <Card
        style={{
          margin: '15px auto',
          width: '210px',
          height: '350px',
          margin: '15px 5px',
          overflow: 'hidden',
          cursor: 'pointer',
        }}
        cover={
          <Image
            preview={false}
            src={movie.hinhAnh}
            style={{
              width: '100%',
              height: '220px',
              margin: '0px auto',
              padding: '0px 0',
              borderRadius: '0',
            }}
          />
        }>
        <div className='h-10'>
          <h1 className='text-base font-medium'>
            {movie.tenPhim.toUpperCase()}
          </h1>
          <span class='material-symbols-outlined play_btn'>play_circle</span>
        </div>
      </Card>
    </>
  );
}
