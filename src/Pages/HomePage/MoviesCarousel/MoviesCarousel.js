import {useEffect} from 'react';
import {movieService} from '../../../services/movieService';
import {useDispatch} from 'react-redux';
import {GET_MOVIES} from '../../../redux/const/moviesConst';
import MoviesList from '../../Components/MovieList/MoviesList';

export default function MoviesCarousel() {
  const dispatch = useDispatch();

  useEffect(() => {
    movieService
      .getMovie()
      .then((res) => {
        dispatch({
          type: GET_MOVIES,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className='container'>
      <MoviesList />
    </div>
  );
}
