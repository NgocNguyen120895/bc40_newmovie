import React, {useState, useEffect} from 'react';
import {Carousel} from 'antd';
import {movieService} from '../../../services/movieService';
import Modal from './Modal';
import BaseButton from './Button';
import {LeftOutlined, RightOutlined} from '@ant-design/icons';

const NextArrow = ({currentSlide, slideCount, ...props}) => {
  return (
    <button
      style={{
        background: 'red',
        width: 50,
        height: 50,
      }}
      {...props}
      className={
        'slick-next slick-arrow' +
        (currentSlide === slideCount - 1 ? ' slick-disabled' : '')
      }
      aria-hidden='true'
      aria-disabled={currentSlide === slideCount - 1 ? true : false}
      type='button'></button>
  );
};

const PrevArrow = ({currentSlide, slideCount, ...props}) => {
  return (
    <button
      {...props}
      className={
        'slick-prev slick-arrow' + (currentSlide === 0 ? ' slick-disabled' : '')
      }
      aria-hidden='true'
      aria-disabled={currentSlide === 0 ? true : false}
      type='button'>
      <LeftOutlined />
    </button>
  );
};

function MovieCarousel() {
  let [movies, setMovies] = useState([]);

  useEffect(() => {
    movieService
      .getMovie()
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  //Modal pop-up video
  const [openModal, setOpenModal] = useState(false);
  const [modalContent, setModalContent] = useState([]);
  function showModal(e) {
    //handle pop-up modal
    setOpenModal(true);
    setModalContent(e.target.value);
  }

  //function that render movie banner
  let renderBanner = () => {
    return movies.map((movie) => {
      return (
        <>
          <BaseButton value={movie.trailer} onClick={showModal} />
          <img src={movie.hinhAnh} alt='images'/>
        </>
      );
    });
  };

  let setting = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 8,
  };

  return (
    <div
      style={{
        width: '80%',
        height: '50vh',
        margin: 'auto'
      }}>
      <Modal
        content={modalContent}
        open={openModal}
        onClose={() => {
          setOpenModal(false);
        }}
      />
      <Carousel
        {...setting}
        // autoplay
        arrows
        prevArrow={<PrevArrow />}
        nextArrow={<NextArrow />}
        infinite={true}
        slideToShow={5}
        style={{background: '#000', width: '100%'}}
        effect='fade'>
          {renderBanner()}
        </Carousel>
    </div>
  );
}

export default MovieCarousel;
