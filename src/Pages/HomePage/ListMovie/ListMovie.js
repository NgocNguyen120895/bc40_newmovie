import React, {useState} from 'react';
import {movieService} from '../../../services/movieService';
import {useEffect} from 'react';
import MovieItem from './MovieItem';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';

export default function ListMovie() {
  let [movies, setMovies] = useState([]);

  const settings = {
    centerMode: false,
    infinite: false,
    slidesToShow: 3,
    speed: 200,
    rows: 2,
    slidesPerRow: 1,
    dots: true,
    arrows: false,
    centerPadding: '300px',
    variableWidth: false,
  };

  useEffect(() => {
    movieService
      .getMovie()
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderMovies = () => {
    return movies.map((movie) => {
      return <MovieItem movie={movie} key={movie.maPhim} />;
    });
  };

  return (
    <div className='lg:container lg:mx-auto'>
      <Slider
        className='container grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-4'
        {...settings}>
        {renderMovies()}
      </Slider>
    </div>
  );
}
