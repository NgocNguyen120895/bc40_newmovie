import React from 'react';
import './modalStyles.css';

export default function Modal({open, onClose, content}) {
  let autoPlay = '?autoplay=1&mute=1';

  if (!open) return null;
  {
    return (
      <div className='modalOverlay' onClick={onClose}>
        <div className='modal flex items-center justify-center'>
          <div
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              width: 50,
              height: 50,
              backgroundColor: 'red',
              color: 'white',
              fontSize: 30,
              textAlign: 'center',
              cursor: "pointer"
            }}
            onClick={onClose}>
            X
          </div>
          <iframe
            className='rounded pt-1'
            width={'95%'}
            height={'90%'}
            src={content + autoPlay}
          />
        </div>
      </div>
    );
  }
}
