import React from 'react';
import {Card} from 'antd';
import {NavLink} from 'react-router-dom';
const {Meta} = Card;

export default function MovieItem({movie}) {
  return (
    <Card
      className='containerCard px-3 py-3'
      style={{width: '300px', width: 300, height: 'auto', overflow: 'hidden'}}
      hoverable
      size='small'>
      <div
        style={{
          backgroundColor: '#ccc',
          width: '100%',
          height: '400px',
          top: '0px',
          left: '0px',
          zIndex: 1000,
        }}>
        <div
          style={{
            borderRadius: '5px',
            width: '100%',
            height: '100%',
            backgroundImage: `url(${movie.hinhAnh})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
          }}
        />
      </div>
      <Meta
        title={movie.tenPhim}
        description={
          <div className='movieTitle'>
            <NavLink to={`/detail/${movie.maPhim}`}>
              <button className='bg-red-500 text-white w-20 h-8 rounded hover:text-gray-200 hover:bg-red-400'>
                Xem ngay
              </button>
            </NavLink>
          </div>
        }
      />
    </Card>
  );
}
