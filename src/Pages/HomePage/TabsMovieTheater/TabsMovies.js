import React, {useState} from 'react';
import './TabsMovies.css';
import {useEffect} from 'react';
import {movieService} from '../../../services/movieService';

function TabsMovies() {
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        setHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let [heThongRap, setHeThongRap] = useState([]);
  let [thongTinRap, setThongTinRap] = useState([]);

  let renderLogo = () => {
    return heThongRap.map((logo) => {
      return (
        <button
          className='tabs'
          onClick={() => {
            setThongTinRap(logo.lstCumRap);
          }}>
          <span className='images_outter'>
            <img
              style={{width: '100%', height: '100%', objectFit: 'cover'}}
              src={logo.logo}
            />
          </span>
        </button>
      );
    });
  };

  const renderThongTinCumRap = (name) => {
    return name.map((rap, index) => {
      return (
        <button className='mid_section'>
          <p>{rap.tenCumRap}</p>
          <p>{rap.diaChi}</p>
          <span>{'[Chi Tiet]'}</span>
        </button>
      );
    });
  };

  return (
    <div className='container'>
      <div className='bloc-tabs'>{renderLogo()}</div>

      <div className='content_Tabs active-content'></div>

      <div className='rightTab'></div>
    </div>
  );
}

export default TabsMovies;
