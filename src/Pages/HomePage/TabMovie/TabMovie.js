import React, {useEffect, useState} from 'react';
import {movieService} from '../../../services/movieService';
import {Tabs} from 'antd';
import TabItemMovie from './TabItemMovie';

export default function TabMovie() {
  let [heThongRap, setHeThongRap] = useState([]);

  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        setHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderHeThongRap = () => {
    return heThongRap.map((rap) => {
      return {
        key: rap.maHeThongRap,
        label: (
          <div
            style={{
              background: `url(${rap.logo})`,
              content: '',
              width: '45px',
              height: '45px',
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
              backgroundSize: 'cover',
              margin: '15px 0',
            }}>
            {''}
          </div>
        ),
        children: (
          <>
            <Tabs
              tabBarStyle={{
                // border: '1px solid rgba(100, 99, 99, 0.5)',
                paddingLeft: 0,
                width: '40%',
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                overflow: 'hidden',
              }}
              tabPosition='left'
              defaultActiveKey='1'
              items={rap.lstCumRap.map((cumRap) => {
                return {
                  key: cumRap.maCumRap,
                  label: (
                    <button
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        width: '100%',
                        height: '75px',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <h2 className='font-normal text-base text-green-600'>
                        {cumRap.tenCumRap}
                      </h2>
                      <h2 className='font-normal text-black'>
                        {cumRap.diaChi}
                      </h2>
                    </button>
                  ),
                  children: (
                    <>
                      {cumRap.danhSachPhim.map((movie) => {
                        return <TabItemMovie movie={movie} />;
                      })}
                    </>
                  ),
                };
              })}
            />
          </>
        ),
      };
    });
  };

  return (
    <div className='container'>
      <div
        style={{
          width: '60%',
          margin: 'auto',
        }}>
        <Tabs
          tabBarStyle={{
            margin: '0 auto',
            display: 'flex',
            width: '5vw',
            height: '100%',
            margin: '0 auto',
            overflow: 'hidden',
          }}
          tabPosition='left'
          defaultActiveKey='1'
          items={renderHeThongRap()}
        />
      </div>
    </div>
  );
}
