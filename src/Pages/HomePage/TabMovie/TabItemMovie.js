import moment from 'moment/moment';
import React from 'react';
import {NavLink} from 'react-router-dom';

export default function TabItemMovie({movie}) {
  return (
    <div
      className='flex items-center justify-center'
      style={{
        width: 500,
        height: 172,
        borderBottom: '1px solid rgba(100, 99, 99, 0.5)',
      }}>
      <div
        style={{
          background: `url(${movie.hinhAnh})`,
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundSize: '100px 150px',
          content: '',
          width: 100,
          height: 150,
        }}></div>

      <div style={{}}>
        <div className='grid justify-items-start px-5'>
          <h2 className='font-bold text-sm text-center'>
            <span className='bg-red-500 text-center text-white rounded w-10 px-1 m-1'>
              C18
            </span>
            {movie.tenPhim}
          </h2>
        </div>

        <div className='px-5 my-2 grid grid-cols-2 gap-3'>
          {movie.lstLichChieuTheoPhim.slice(0, 4).map((lichChieuTheoPhim) => {
            return (
              <NavLink to={`detail/${movie.maPhim}`}>
                <button className='border-solid border-2 bg-slate-50 p-2 w-30 h-14 rounded text-center'>
                  <h4 className='font-medium text-green-800'>
                    {moment(lichChieuTheoPhim.ngayChieuGioChieu).format(
                      'DD/MM/YYYY ~ hh:mm'
                    )}
                  </h4>
                </button>
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
