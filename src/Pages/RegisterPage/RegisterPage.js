import React from 'react';
import {Button, Checkbox, Form, Input, message} from 'antd';
import {userService} from '../../services/userService';
import {useNavigate} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faRegistered} from '@fortawesome/free-solid-svg-icons';

const formItemLayout = {
  labelCol: {
    xs: 12,
    sm: 12,
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 4,
    },
    sm: {
      span: 16,
      offset: 4,
    },
  },
};
const style = {
  style: {
    width: 300,
  },
};

const RegisterPage = () => {
  const [form] = Form.useForm();
  let navigate = useNavigate();

  const onFinish = (values) => {
    userService
      .register(values)
      .then((res) => {
        message.success('Đăng ký thành công!');
        navigate('/login');
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  return (
    <div className='grid place-content-center bg-opacity'>
      <div
        className='content bg-slate-100 rounded border-4 border-red-800 space-y-10 flex items-center justify-center'
        style={{height: 600, width: 500}}>
        <Form
          {...formItemLayout}
          form={form}
          name='register'
          onFinish={onFinish}
          style={{
            maxWidth: 600,
          }}
          scrollToFirstError>
          <div className='py-5'>
            <FontAwesomeIcon
              icon={faRegistered}
              style={{color: '#ff0000', width: 60, height: 60}}
            />
            <h1 className='text-xl font-bold text-red-500'>Đăng Ký</h1>
          </div>
          <Form.Item
            name='email'
            rules={[
              {
                type: 'email',
                message: 'Cú pháp của email chưa đúng, ví dụ: abc@cdf.com',
              },
              {
                required: true,
                message: 'Bạn chưa nhập E-mail!',
              },
            ]}>
            <Input {...style} placeholder='Điền email' />
          </Form.Item>

          <Form.Item
            name='taiKhoan'
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập tên tài khoản!',
              },
            ]}>
            <Input {...style} placeholder='Tên tài khoản' />
          </Form.Item>

          <Form.Item
            name='password'
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập mật khẩu!',
              },
            ]}
            hasFeedback>
            <Input.Password {...style} placeholder='Mật khẩu' />
          </Form.Item>

          <Form.Item
            name='matKhau'
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập xác nhận mật khẩu',
              },
              ({getFieldValue}) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error('Nhập lại mật khẩu chưa chính xác!')
                  );
                },
              }),
            ]}>
            <Input.Password {...style} placeholder='Nhập lại mật khẩu' />
          </Form.Item>

          <Form.Item
            name='hoTen'
            tooltip='What do you want others to call you?'
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập họ và tên!',
                whitespace: true,
              },
            ]}>
            <Input placeholder='Họ và tên' {...style} />
          </Form.Item>

          <Form.Item
            name='soDt'
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập số điện thoại!',
              },
            ]}>
            <Input placeholder='Số điện thoại' {...style} />
          </Form.Item>

          <Form.Item
            name='agreement'
            valuePropName='checked'
            rules={[
              {
                validator: (_, value) =>
                  value
                    ? Promise.resolve()
                    : Promise.reject(
                        new Error(
                          'Cần chấp thuận các quy định và điều khoản khi đăng ký'
                        )
                      ),
              },
            ]}
            {...tailFormItemLayout}>
            <Checkbox>
              Tôi đồng ý với các <a href='/'>điều khoản</a>
            </Checkbox>
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button className='bg-red-500' type='primary' htmlType='submit'>
              Đăng ký
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
export default RegisterPage;
