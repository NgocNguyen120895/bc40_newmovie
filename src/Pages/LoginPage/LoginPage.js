import React from 'react';
import {Button, Checkbox, Form, Input, message} from 'antd';
import '../LoginPage/LoginPage.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faUser} from '@fortawesome/free-solid-svg-icons';
import {userService} from '../../services/userService';
import {localUserService} from '../../services/localService';
import {useNavigate} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {USER_LOGIN} from '../../redux/const/userConst';

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinish = (values) => {
    console.log('Success:', values);
    userService
      .postLogin(values)
      .then((res) => {
        message.success('Đăng nhập thành công!');
        localUserService.set(res.data.content);
        navigate('/');
        let action = {
          type: USER_LOGIN,
          payload: res.data.content,
        };
        dispatch(action);
      })
      .catch((err) => {
        console.log(err);
        message.error('Đăng nhập thất bại!');
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className='grid place-content-center bg-opacity'>
      <div
        className='content bg-slate-100 rounded border-4 border-red-800 space-y-10 flex items-center justify-center'
        style={{height: 400, width: 500}}>
        <Form
          className='flex flex-col items-center justify-center'
          name='basic'
          labelCol={{
            span: 8,
          }}
   
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete='off'>
          <FontAwesomeIcon
            className='py-5'
            icon={faUser}
            style={{color: '#db1414', height: 40, width: 40}}
          />
          <h2 className='font-medium text-red-600 py-2'>Đăng Nhập</h2>
          <Form.Item
            name='taiKhoan'
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
            style={{width: 300}}>
            <Input placeholder='Username' />
          </Form.Item>

          <Form.Item
            name='matKhau'
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
            style={{width: 300}}>
            <Input.Password placeholder='Password' />
          </Form.Item>

          <Form.Item name='remember' valuePropName='checked'>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item>
            <Button
              className='bg-red-400 w-28'
              type='primary'
              htmlType='submit'>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
