import React from 'react';
import {Button, Dropdown, Space} from 'antd';
import {NavLink} from 'react-router-dom';
import UserMenu from './UserMenu';

const handleClickScroll = (section) => {
  const element = document.getElementById(section);
  if (element) {
    element.scrollIntoView({behavior: 'smooth'});
  }
};

const items = [
  {
    label: <UserMenu />,
    key: '0',
  },
];

export default function HeaderTablet() {
  return (
    <div className='md:w-full md:h-22'>
      <div className='flex items-center justify-between'>
        <NavLink to='/'>
          <h1 className='font-extrabold text-4xl text-red-400 animate-bounce hover:animate-bounce hover:text-white'>
            MOVIE FLIX
          </h1>
        </NavLink>

        <div className='grid grid-cols-4 gap-5'>
          <button
            onClick={() => {
              handleClickScroll('listMovie');
            }}
            className='text-red-500 font-medium hover:text-white hover:duration-300'>
            Lịch Chiếu
          </button>

          <button
            onClick={() => handleClickScroll('cumRap')}
            className='text-red-500 font-medium hover:text-white hover:duration-300'>
            Cụm Rạp
          </button>
        </div>

        <Dropdown
          menu={{
            items,
          }}
          trigger={['click']}>
          <a onClick={(e) => e.preventDefault()}>
            <Button className='bg-red-500 text-slate-300 hover:bg-red-400 hover:font-medium'>
              Menu
            </Button>
          </a>
        </Dropdown>
      </div>
    </div>
  );
}
