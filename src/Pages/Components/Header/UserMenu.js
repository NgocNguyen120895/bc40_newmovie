import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRightToBracket, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { useSelector } from 'react-redux';
import { localUserService } from "../../../services/localService"



export default function UserMenu() {

    let userInfo = useSelector(state => {
        return state.userReducer.userInfo
    })

    let handleLogout = () => {
        //Remove userInformations from local storage
        localUserService.remove();
        //Re-direct user to main page
        window.location.href = "/";

        //using window.location.reload() --> alternative option
    }

    function render() {

        if (!userInfo) {
            return (
                <>
                    <div className='flex flex-row'>
                        <NavLink to="/login" className="flex flex-row items-center">
                            <FontAwesomeIcon className='text-red-400' icon={faRightToBracket} />
                            <button className='px-5 flex items-center'>
                                <h2 className='font-bold text-red-500'>Đăng Nhập</h2>
                            </button>
                        </NavLink>
                        <span>||</span>
                        <NavLink to="/register" className="flex flex-row items-center">
                            <FontAwesomeIcon className='text-red-400 mx-3' icon={faUserPlus} />
                            <button className='px-5 flex items-center'>
                                <h2 className='font-bold text-red-500'>Đăng Ký</h2>
                            </button>
                        </NavLink>
                    </div>
                </>
            )
        } else {
            return (
                <>
                    <div>Xin chào,
                        <span> </span>
                        <span>{userInfo.hoTen}</span>
                    </div>
                    <div>
                        <button onClick={handleLogout} className='h-8 w-20 rounded border-2 border-red-500 text-red-500'>Đăng Xuất</button>
                    </div>
                </>
            )
        }

    };

    return (
        <>
            {render()}
        </>
    )
}
