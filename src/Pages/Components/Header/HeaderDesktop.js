import React from 'react';
import UserMenu from './UserMenu';
import {NavLink, useNavigate} from 'react-router-dom';

const handleClickScroll = (section) => {
  const element = document.getElementById(section);
  if (element) {
    element.scrollIntoView({behavior: 'smooth'});
  }
};

export default function HeaderDesktop() {
  let navigate = useNavigate();

  const handleChangePage = (url) => {
    navigate(url);
  };

  return (
    <div className='w-full h-15'>
      <div className='mx-auto flex items-center justify-around'>
        <NavLink to='/'>
          <h1 className='font-extrabold text-4xl text-red-400 animate-bounce hover:animate-bounce hover:text-white'>
            MOVIE FLIX
          </h1>
        </NavLink>
        <div>
          <button
            onClick={() => {
              handleChangePage('/');
            }}
            className='text-red-500 font-medium hover:text-white hover:duration-300 px-5'>
            Lịch Chiếu
          </button>
          <button
            onClick={() => handleClickScroll('cumRap')}
            className='text-red-500 font-medium hover:text-white hover:duration-300 px-5'>
            Cụm Rạp
          </button>
          <button
            onClick={() => handleChangePage('/news')}
            className='text-red-500 font-medium hover:text-white hover:duration-300 px-5'>
            Tin Tức
          </button>
          <button
            onClick={() => handleChangePage('/contact')}
            className='text-red-500 font-medium hover:text-white hover:duration-300 px-5'>
            Liên Hệ
          </button>
        </div>
        <div>
          <UserMenu />
        </div>
      </div>
    </div>
  );
}
