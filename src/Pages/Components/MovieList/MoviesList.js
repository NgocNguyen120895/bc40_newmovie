import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import CardMovie from '../../HomePage/MoviesCarousel/CardMovie';
import {Col, Row, Carousel, Card} from 'antd';
import {RightOutlined, LeftOutlined} from '@ant-design/icons';
import './MoviesList.css';
import {GET_TRAILER} from '../../../redux/const/moviesConst';
import ModalTrailer from '../CarouselBanner/ModalTrailer';
import {useNavigate} from 'react-router-dom';

export default function MoviesList() {
  const moviesList = useSelector((state) => state.moviesReducer.moviesList);
  const navigate = useNavigate();
  let [openModal, setOpenModal] = useState(false);

  let dispatch = useDispatch();

  const renderMovies = (a, b) => {
    return moviesList.slice(a, b).map((item, index) => {
      return (
        <Col>
          <div
            onClick={() => {
              dispatch({
                type: GET_TRAILER,
                payload: item.trailer,
              });
              setOpenModal(true);
            }}>
            <CardMovie movie={item} />
          </div>
          <button
            className='booking_button'
            onClick={() => {
              navigate(`/detail/${item.maPhim}`);
            }}>
            Đặt Vé
          </button>
        </Col>
      );
    });
  };

  return (
    <div className='carousel_background'>
      <ModalTrailer
        open={openModal}
        onclose={() => {
          setOpenModal(false);
        }}
      />
      <Carousel
        dots={true}
        dotPosition='bottom'
        arrows={true}
        prevArrow={<LeftOutlined />}
        nextArrow={<RightOutlined />}>
        <Row>
          <Row className='items-center justify-center'>
            {renderMovies(0, 4)}
          </Row>
          <Row className='items-center justify-center'>
            {renderMovies(0, 4)}
          </Row>
        </Row>
        <Row>
          <Row className='items-center justify-center'>
            {renderMovies(0, 4)}
          </Row>
          <Row className='items-center justify-center'>
            {renderMovies(0, 4)}
          </Row>
        </Row>
      </Carousel>
    </div>
  );
}
