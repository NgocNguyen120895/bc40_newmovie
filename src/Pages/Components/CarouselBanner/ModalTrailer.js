import React, {useEffect} from 'react';
import './carousel.css';
import {useSelector} from 'react-redux';

export default function ModalTrailer(props) {
  let autoPlay = '?autoplay=1&mute=1';

  let data = useSelector((state) => {
    return state.moviesReducer.moviesTrailer;
  });

  const useDisableBodyScroll = (open) => {
    useEffect(() => {
      if (open) {
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.overflow = 'unset';
      }
    }, [open]);
  };
  useDisableBodyScroll(props.open);

  if (!props.open) {
    return null;
  }

  return (
    <div className='modal_overlay' onClick={props.onclose}>
      <div className='modal'>
        <div className='inner_modal'>
          <button>
            <span class='material-symbols-outlined'>close</span>
          </button>
          {
            <iframe
              width={'95%'}
              height={'90%'}
              title='YouTube video player'
              frameborder='0'
              allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share'
              src={
                data != null
                  ? data + autoPlay
                  : 'https://www.youtube.com/embed/TdiVT8A7RB0'
              }
            />
          }
        </div>
      </div>
    </div>
  );
}
