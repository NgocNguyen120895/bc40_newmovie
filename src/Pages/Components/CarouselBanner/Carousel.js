import React, {useState} from 'react';
import ItemCarousel from './ItemCarousel';
import ModalTrailer from './ModalTrailer';
import './carousel.css';

const Carousel = (props) => {
  const [activeIndex, setIndex] = useState(0);

  const {data} = props;
  const length = data.length;

  const updateIndex = (newIndex) => {
    if (newIndex < 0) {
      newIndex = 0;
    } else if (newIndex >= data.length) {
      newIndex = data.length - 1;
    }
    setIndex(newIndex);
  };

  const prev = () => {
    const newIndex = activeIndex - 1;
    setIndex(newIndex < 0 ? length - 1 : newIndex);
  };

  const next = () => {
    const newIndex = activeIndex + 1;
    setIndex(newIndex >= length ? 0 : newIndex);
  };

  let [openModal, setOpenModal] = useState(false);
  let [content, setContent] = useState({});

  return (
    <div className='carousel'>
      <div
        className='inner'
        style={{
          transform: `translate(-${activeIndex * 100}%)`,
        }}>
        {data.map((item) => {
          return (
            <>
              <ItemCarousel
                item={item}
                setOpenModal={setOpenModal}
                setContent={setContent}
              />
            </>
          );
        })}
      </div>

      {/* Buttons and idicators for banner  */}
      <div className='carousel_buttons'>
        <button onClick={prev} className='button_arrows'>
          <span class='material-symbols-outlined symbol'>arrow_back_ios</span>
        </button>
        <div className='indicators'>
          {data.map((_, index) => {
            return (
              <button
                className='indicator_button'
                onClick={() => {
                  updateIndex(index);
                }}>
                {index === activeIndex ? (
                  <span class='material-symbols-outlined symbol_indicator'>
                    radio_button_checked
                  </span>
                ) : (
                  <span class='material-symbols-outlined symbol_indicator'>
                    radio_button_unchecked
                  </span>
                )}
              </button>
            );
          })}
        </div>
        <button onClick={next} className='button_arrows'>
          <span class='material-symbols-outlined symbol'>
            arrow_forward_ios
          </span>
        </button>
      </div>

      {/* pop up modal */}
      <ModalTrailer
        open={openModal}
        onclose={() => {
          setOpenModal(false);
        }}
        content={content}
      />
    </div>
  );
};

export default Carousel;
