import React from 'react';
import './carousel.css';
import {useDispatch} from 'react-redux';
import {GET_TRAILER} from '../../../redux/const/moviesConst';
export default function ItemCarousel(props) {
  let {item, setOpenModal} = props;
  let dispatch = useDispatch();

  const setOpen = (state) => {
    setOpenModal(state);
  };

  return (
    <>
      <div className='carousel_item'>
        <div
          className='background_image'
          style={{
            background: `url(${item.hinhAnh})`,
          }}></div>
        <button
          className='button_modal'
          onClick={() => {
            setOpen(true);
            dispatch({
              type: GET_TRAILER,
              payload: item.trailer,
            });
          }}>
          <span class='material-symbols-outlined'>play_circle</span>
        </button>
      </div>
    </>
  );
}
