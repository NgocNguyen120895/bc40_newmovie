import React, {useEffect, useState} from 'react';
import {movieService} from '../../../services/movieService';
import Carousel from './Carousel';
import ModalTrailer from './ModalTrailer';

export default function BannerMovies() {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    movieService
      .getBanner()
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      <Carousel data={movies} />
      <ModalTrailer data={movies} />
    </>
  );
}
