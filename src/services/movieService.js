import axios from 'axios';
import {headersConfig, BASE_URL} from './config';

export const movieService = {
  getBanner: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
      method: 'GET',
      headers: {
        TokenCybersoft: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8`,
      },
    });
  },
  getMovie: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
  getMovieByTheater: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP04`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
  getMovieDetail: (movieId) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieId}`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
  getMovieBanner: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
};
