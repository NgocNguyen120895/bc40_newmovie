import axios from 'axios';
import { BASE_URL, headersConfig } from './config';


export const userService = {

    postLogin: (userFormInput) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method: "POST",
            data: userFormInput,
            headers: headersConfig(),
        })
    },
    register: (userFormInput) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
            method: "POST",
            data: userFormInput,
            headers: headersConfig(),
        })
    }

}