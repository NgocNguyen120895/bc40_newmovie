import axios from "axios";
import { headersConfig, BASE_URL } from "./config";


export const userService = (userFromForm) => {
    return axios({
        url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
        method: "POST",
        data: userFromForm,
        headers: headersConfig()
    })
}