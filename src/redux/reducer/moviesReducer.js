import {GET_MOVIES, GET_TRAILER} from '../const/moviesConst';

let initState = {
  moviesList: [],
  moviesTrailer: null,
};

export const moviesReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_MOVIES: {
      return {...state, moviesList: action.payload};
    }
    case GET_TRAILER: {
      return {...state, moviesTrailer: action.payload};
    }
    default:
      return state;
  }
};
